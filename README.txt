*******************************************************************************

blogstats

Description:
-------------------------------------------------------------------------------

  This module allows site developers to track blog and comment statistics. 
  Statistics include total posts and comments, authors with the most posts, 
  authors whose posts have received the most comments, and blog posts which 
  have generated the most comments. Statistics can be displayed on an annual, 
  monthly, or quarterly basis.



Installation & Use:
-------------------------------------------------------------------------------

1.  Enable module in module list located at administer > build > modules.
2.  Go to admin/settings/blogstats to select the type and number of categories you would like to view.
3.  Go to admin/reports/blogstats to view your statistics.
4.  Within a timeframe, click on "Update" to refresh the data.


Note:
-------------------------------------------------------------------------------
This is the first iteration of this module, and continued development is 
expected. Further iterations may include tracking post and comment statistics 
for blog categories and tags associated with blogs, as well as hooking into 
google analytics to include blog posts with the move pageviews.



Author:
-------------------------------------------------------------------------------

Zeth Lietzau <zlietzau@denverlibrary.org>
http://drupal.org/user/866546

