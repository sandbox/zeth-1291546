<?php	

//FUNCTION FOR UPDATING DATA IN THE DATABASE
function blogstats_update_data($timeframe=false) {
	//KILL THIS FUNCTION IF THERE'S NO TIMEFRAME SELECTED
	if (!$timeframe) {return;}
	
	//PULL APART TIMEFRAME INTO USABLE PIECES & MAKE SURE IT'S A REASONABLE YEAR
	$timeframesplit = explode("-", $timeframe);
	$timeyear = intval($timeframesplit[0]);
	if (sizeof($timeframesplit) != 2 || $timeyear < 1900) {return;}
	
	//SET UP APPROPRIATE TIME FRAME 
	// FOR ANNUAL
	if ($timeframesplit[1][0] == "0") {
		$createstart = strtotime("1 January $timeyear 00:00:00");
		$createend = strtotime("31 December $timeyear 23:59:59");
	}
	// FOR MONTHLY
	elseif ($timeframesplit[1][0] == "m") {
		$thismo = intval(str_replace("m", "", $timeframesplit[1]));
		if ($thismo < 1 || $thismo > 12) {return;}
		$createstart = strtotime("$timeyear-$thismo-1 00:00:00");
		$numdays = date("t", $createstart);
		$createend = strtotime("$timeyear-$thismo-$numdays 23:59:59");
	}
	
	// FORM QUARTERLY
	elseif ($timeframesplit[1][0] == "q") {
		$thisq = intval(str_replace("q", "", $timeframesplit[1]));
		if ($thisq < 1 || $thisq > 4) {return;}
		switch ($thisq) {
			case 1: 
				$createstart = strtotime("1 January $timeyear 00:00:00");
				$createend = strtotime("31 March $timeyear 23:59:59");
				break;
			case 2: 
				$createstart = strtotime("1 April $timeyear 00:00:00");
				$createend = strtotime("30 June $timeyear 23:59:59");
				break;
			case 3: 
				$createstart = strtotime("1 July $timeyear 00:00:00");
				$createend = strtotime("30 September $timeyear 23:59:59");
				break;
			case 4: 
				$createstart = strtotime("1 October $timeyear 00:00:00");
				$createend = strtotime("31 December $timeyear 23:59:59");
				break;
		}
	}
	else {return;} //ESCAPE FUNCTION IF THE TIMEFRAME IS NOT FORMATTED FOR AN APPROPRIATE TIME SEGMENT
	
	//PULL INFRASTRUCTURE DATA (FROM blogstats.module)
	$timeframes = unserialize(BLOG_STATS_TIMEFRAMES);
	$blogstats_sections = unserialize(BLOG_STATS_SECTIONS);	

	//LOOP THROUGH SECTIONS & SET VARIABLES TO MAKE SURE WE UPDATE APPROPRIATE INFORMATION
	foreach ($blogstats_sections as $secname => $sec) {
		$$sec["checkvariable"] = false;
		$pieces[$sec["arr"]] = variable_get($secname."_posts_comments", "");
		if ($sec["posts"]) {$titles[$sec["arr"]][$secname."_posts"] = "Posts";}
		if ($sec["comments"]) {$titles[$sec["arr"]][$secname."_comments"] = "Comments";}
	
	/*
		$$var = true sets each piece (posts, comments) to true if it's been selected
		if any user pieces have been selected, the $availableusers array is set up to tie username to id
		USERNAME DATA IS STORED IN THE $words ARRAY
	*/	
		if ($pieces[$sec["arr"]]) {
			foreach ($pieces[$sec["arr"]] as $var => $val) {
				if ($var === $val) {
					$$sec["checkvariable"] = true;
					$$var = true;
					//GRAB USER NAMES IF IT HASN'T BEEN DONE
					if ($secname == "users") {
						if (sizeof($words[$secname]) == 0) {
							$sql = "SELECT `name`, `uid` FROM {users}";
							$result = db_query(db_rewrite_sql($sql));
							while($row=db_fetch_object($result)) {
								$words[$secname][$row->uid] = $row->name;
							}						
						}
					}
				}
			}
		}
	}	

	//PULL BLOG POSTS AND SET UP ARRAYS WITH BLOG POST INFORMATION		
	$result = db_query("SELECT `nid`, `uid`, `created`, `title` FROM {node} WHERE `type` = 'blog' AND `created` BETWEEN '%d' AND '%d'", $createstart, $createend);	
	if (db_affected_rows($result) > 0) {
		while($row=db_fetch_object($result)) {
			//GET BLOG USER IDS
			$blog_author[$row->nid] = $row->uid;
			//COUNT NUMBER OF USER POSTS FOR THIS TIMEFRAME
			if ($useron) {$usersposts[$row->uid]++;}
			//ATTACH BLOG TITLE TO NODE IT
			if ($blogposton) {$words["blogposts"][$row->nid] = t($row->title);}
			$blogposts++;
		}	
	}	
	
	// PULL COMMENTS AND SET UP ARRAYS WITH COMMENT INFORMATION
	$result = db_query("SELECT `timestamp`, `nid`, SUM(IF(`uid`>0, 1, 0)) as 'u', SUM(IF(`uid`=0, 1, 0)) as 'a' FROM {comments} WHERE `timestamp` >= %d AND `timestamp` <= %d GROUP BY `nid`", $createstart, $createend);	
	if (db_affected_rows($result) > 0) {
		while($row=db_fetch_object($result)) {
			//ADD ANONYMOUS AND USER COMMENTS FOR TOTAL COMMENTS
			$thissum = $row->u + $row->a;
			/*Set up blog comments array for all comments and anonymous for each timeframe*/					
			$blogcomments["total"] += $thissum;
			$blogcomments["anon"] += $row->a;
			
			/*Set up comments array for each individual blog post*/
			$blogpostscomments[$row->nid]["total"] += $thissum;
			$blogpostscomments[$row->nid]["anon"] += $row->a;
			
			//IF THE TITLE & AUTHOR INFORMATION THIS BLOG POST HAS NOT ALREADY BEEN CAPTURED, GATHER IT
			if ($words["blogposts"] && !array_key_exists($row->nid, $words["blogposts"])) {
				$result2 = db_query("SELECT `title`, `uid` FROM {node} WHERE `nid` = %d", $row->nid);
				while($row2=db_fetch_object($result2)) {
					$words["blogposts"][$row->nid] = $row2->title;
					$blog_author[$row->nid] = $row2->uid;
				}
			}
			
		}
	}
	
	// SET UP $datainserts ARRAY TO HOLD INFORMATION FOR ADDING DATA TO THE DATABASE\
	// AND INSERT SUMMARY DATA
	$datainserts = array(
		"allposts" => array(
			"category" => "total", 
			"subcategory" => "posts", 
			"number" => $blogposts, 
			"title" => ""
		),
		"allcomments" => array(
			"category" => "total", 
			"subcategory" => "comments", 
			"number" => $blogcomments["total"], 
			"title" => ""
		), 
		"allanoncomments" => array(
			"category" => "total", 
			"subcategory" => "anoncomments", 
			"number" => $blogcomments["anon"], 
			"title" => ""
		)		
	);
	
	// USER INFORMATION
	if ($usersposts) {
		// LOOP THROUGH USER POSTS AND ADD ITEMS TO THE $datainserts ARRAY 
		foreach ($usersposts as $thisu => $val) {
			$datainserts["user-$thisu"] = array(
				"category" => "users", 
				"subcategory" => posts, 
				"number" => $val, 
				"title" => $words["users"][$thisu]
			
			);
		}
	}

	// BLOG POST COMMENT INFORMATION
	if ($blogpostscomments) {
		// LOOP THROUGH BLOG POST COMMENTS AND ADD ITEMS TO THE $datainserts ARRAY
		foreach ($blogpostscomments as $n => $arr) {
			//DO BLOG POST
			$datainserts["post-$n"] = array( 
				"category" => "blogposts", 
				"subcategory" => "comments", 
				"number" => $arr["total"], 
				"title" => $words["blogposts"][$n]
			);
			$authorcomments[$blog_author[$n]] += $arr["total"];
		}
	}
	
	// AUTHOR/COMMENTS INFORMATION
	if ($authorcomments) {
		foreach ($authorcomments as $author => $val) {
			$datainserts["userc-$author"] = array( 
				"category" => "users", 
				"subcategory" => "comments", 
				"number" => $val, 
				"title" => $words["users"][$author]
			);
		}
	}

	//CLEAR OUT DATABASE FOR THIS TIMEFRAME BEFORE ADDING NEW DATA
	$result = db_query("DELETE FROM {blogstats} WHERE `timeframe` = '%s'", $timeframe);
	//INSERT EACH DATA PIECE FOR THIS TIMEFRAME
	if ($datainserts) {
		foreach ($datainserts as $key => $arr) {
			$thistitle = "";
			$thiscat = $arr["category"];
			$thissubcat = $arr["subcategory"];
			$thisnum = $arr["number"];
			$thistitle = $arr["title"];
			_blogstats_update_data($timeframe, $thiscat, $thissubcat, $thisnum, true, $thistitle); 
		}
	}
	
}

function _blogstats_update_data ($timeframe, $category, $subcategory, $number, $new = false, $title='') {
		$result = db_query("INSERT INTO {blogstats} (`timeframe`, `category`, `subcategory`, `title`, `number`, `timestamp`) VALUES 
				('%s', '%s', '%s', '%s', %d, now())", $timeframe, $category, $subcategory, $title, $number);	
}



