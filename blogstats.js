/* 
	EXTEND JQUERY TO ADD A getUrlVars FUNCTION WHICH ALLOWS US TO GRAB 
	URL VARIABLES AND LATER DETERMINE WHETHER THE process=on
*/
$.extend({
  getUrlVars: function(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name){
    return $.getUrlVars()[name];
  }
});

$(function() {
	//HIDE HIDDEN DIVS - DONE IN JAVASCRIPT SO USERS WITH JS OFF CAN SEE ALL THE INFO
	$(".starthidden").css("display", "none");
	
	//SLIDE DOWN TIME PERIOD DIVS WHEN CLICKED
	$(".showchild").click(function(e) {
		e.preventDefault();
		var childid = $(this).attr("id").replace("show_", "");
		$("#"+childid).slideToggle();
	})
	
	//IF DATA HAS JUST BEEN UPDATED, OPEN THE APPROPRIATE DIV AND SLIDE TO IT
	if ($.getUrlVar("process") == "on") {
		var timeframe = $.getUrlVar("timeframe");
		var tsplit = timeframe.split("-");
		var topen = tsplit[1];
		if (topen == 0) {
			$("#blogstats_annual").css("display", "block");
		}
		if (topen.charAt(0) == "m") {
			$("#blogstats_monthly").css("display", "block");
			
		}
		if (topen.charAt(0) == "q") {
			$("#blogstats_quarterly").css("display", "block");
		}
		var id = "th_"+topen;
		$('html,body').animate({scrollTop: $("#"+id).offset().top}, 'slow')
	}
})